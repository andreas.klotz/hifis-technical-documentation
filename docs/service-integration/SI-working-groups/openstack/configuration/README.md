# Openstack configuration

This snippet for a mapping was taken from an OpenStack configuration for
the EGI federated cloud. We will adjust the tested and working urn
scheme from Helmholtz AAI later. Your input is welcome.

```
     {
         "local": [
             {
                 "user": {
                     "domain": { "id": "default" },
                     "name": "{0}_egiID"
                 },
                 "group": {"id": "222333xxx"}
             }
         ],
         "remote": [
             {"type": "HTTP_OIDC_SUB"},
             {
                 "type": "HTTP_OIDC_ISS",
                 "any_one_of": ["https://aai-demo.egi.eu/oidc/"]
             },
             {
                 "type": "OIDC-eduperson_entitlement",
                 "regex": true,
                 "any_one_of": 
["^urn:mace:egi.eu:group:opencoast.eosc-hub.eu:role=vm_operator#.+$"]
             }
         ]
     },
```

Examples for urn:mace"egi.eu" groups are:

```
urn:mace:egi.eu:aai.egi.eu:member@dteam
urn:mace:egi.eu:group:dteam:role=member#aai.egi.eu
urn:mace:egi.eu:aai.egi.eu:vm_operator@dteam
urn:mace:egi.eu:group:dteam:role=vm_operator#aai.egi.eu
urn:mace:egi.eu:aai.egi.eu:member@vo.indigo-datacloud.eu
urn:mace:egi.eu:group:vo.indigo-datacloud.eu:role=member#aai.egi.eu
urn:mace:egi.eu:aai.egi.eu:vm_operator@vo.indigo-datacloud.eu
urn:mace:egi.eu:group:vo.indigo-datacloud.eu:role=vm_operator#aai.egi.eu
urn:mace:egi.eu:aai.egi.eu:member@opencoast.eosc-hub.eu
urn:mace:egi.eu:group:opencoast.eosc-hub.eu:role=member#aai.egi.eu
urn:mace:egi.eu:aai.egi.eu:vm_operator@opencoast.eosc-hub.eu
urn:mace:egi.eu:group:opencoast.eosc-hub.eu:role=vm_operator#aai.egi.eu
```

See more examples in the Appendix of the [AARC G002
Specification](https://aarc-project.eu/guidelines/aarc-g002/).
