# Service Integration - OpenStack - Technical-Platform overview

Technical-Platform overview for service **OpenStack**

Content:

* [Accounting](accounting.md)
* [META-Data](metadata.md)
* [Monitoring](monitoring.md)