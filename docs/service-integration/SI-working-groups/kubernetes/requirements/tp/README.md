# Service Integration - Kubernetes - Technical-Platform overview

Technical-Platform overview for service **Kubernetes**

Content:

* [Accounting](accounting.md)
* [META-Data](metadata.md)
* [Monitoring](monitoring.md)