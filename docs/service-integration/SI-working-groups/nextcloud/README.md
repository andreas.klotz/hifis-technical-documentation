# Service Integration - Nextcloud - Requirements overview

Requirements overview for service **Nextcloud**

Content:

* [Configuration](configuration/)
* [Documentation](documentation/)
* [Requirements](requirements/)
