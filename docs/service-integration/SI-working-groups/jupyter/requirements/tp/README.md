# Service Integration - Jupyter - Technical-Platform overview

Technical-Platform overview for service **Jupyter**

Content:

* [Accounting](accounting.md)
* [META-Data](metadata.md)
* [Monitoring](monitoring.md)
