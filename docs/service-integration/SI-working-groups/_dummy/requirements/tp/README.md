# Service Integration - _dummy - Technical-Platform overview

Technical-Platform overview for service **_dummy**

Content:

* [Accounting](accounting.md)
* [META-Data](metadata.md)
* [Monitoring](monitoring.md)
