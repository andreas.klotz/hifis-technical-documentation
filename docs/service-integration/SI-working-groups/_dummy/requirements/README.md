# Service Integration - _dummy - Requirements overview

Requirements overview for service **_dummy**

Content:

* [Licensing](licensing)
* [Software](software)
* [Technical-Platform](tp)
