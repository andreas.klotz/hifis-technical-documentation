# Service Integration - Overview

Documentation for working group Service Integration (SI) in HIFIS.

!!! info
    Note that this documentation will be continuously updated in the course of building the Helmholtz Cloud.

### Content

* [Working Groups for various Services / Softwares](SI-working-groups/)
* [Helmholtz Cloud Agent](hca/)
* [List of connected (pilot) Services](../cloud-services/list-of-services.md)

### Architecture

The currently envisioned overall architecture of the Helmholtz Cloud components is depicted below.
The architecture and several components are yet under development and subject to change.

![HCA_architecture](graphs/HIFIS_Cloud_Architecture.svg)
