# Technical Platform - Overview

Documentation for the technical platform in HIFIS.

!!! warning
    Note that this documentation is preliminary and will be updated approx. end of 2020 in the course of building the Helmholtz Cloud.

* [Whitepaper](TP_Whitepaper.md)
* [UI-Mockups](TP_Mockups.md)
* [Developer Manual](TP_DeveloperManual.md)
