# Helmholtz Cloud - Service Portfolio

The following sites include:

## Creation of the initial Service Portfolio 
* [How the service list as a data basis for the service selection was created](initial-service-portfolio/how-the-service-list-was-created.md)
* [How the evaluation criteria used in the service selection were defined](initial-service-portfolio/how-services-are-evaluated.md)
* [How the service selection process was performed](initial-service-portfolio/how-services-are-selected.md), including the corresponding results

## Service Portfolio Management
* [List of Services which are currently part of the Service Portfolio](service-portfolio-management/current-services-in-portfolio.md)
* [Service Portfolio Review documentations](service-portfolio-management/service-portfolio-review-documentations.md)
* for the handling of Service Portfolio, please have a look at the [Process Framework](../process-framework/Chapter-Overview.md)
