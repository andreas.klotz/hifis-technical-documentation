# Documentations of Service Portfolio Reviews

| Review No. | Review Category | Review Type |Starting in | Completed in | Summary of Results |Documentation |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| 1 | Regular Review | Services in Portfolio <br> Service selection criteria <br> Portfolio processes | 01/2022 | 03/2022 | - Remove 7 services from Service Portfolio since they were withdrawn by the Service Provider <br> - Adapt service type wording for former “HIFIS Basis Services” to “Helmholtz Cloud core services” <br> - Adapt criteria descriptions/requirements <br> - Adapt field descriptions in Application form as a result of collected experiences during review <br> - Adapt Service Canvas as a result of collected experiences and for better structure | [Link to Documentation](../Corresponding-files/HIFIS-Cloud-SP_Regular-Review-01-03_2022-documentation.pdf) |
