# Current Services in the Service Portfolio

Last update: 2022-03-17.

The Helmholtz Cloud Service Portfolio includes:

- the **Service Pipeline**: includes all Services which passed the Exclusion criteria (asked for in the Application form) OR are part of the initial service portfolio -  but have not yet been fully integrated into Helmholtz Cloud.
- the **Service Catalogue** includes all services that are currently provided/ in operation. In the context of Helmholtz Cloud, the Service Catalogue is represented by the services offered in Helmholtz Cloud Portal (as "pilots" until the contract is signed by all Helmholtz centers)
- the **Retired services** are the services which were once operated but aren't anymore. In the context of Helmholtz Cloud, the retired Services are not visible in Helmholtz Cloud Portal anymore, but still exist as part of the service database (Plone) with the status "Retired".

## Service Pipeline

| Service Name | Service Provider |
| ------ | ------ |
| AWI Marketplace | AWI |
| GPU compute Service | HZDR |
| JupyterHub | HMGU |
| Ocean & climate Sensor Management | AWI |
| OpenStack | KIT |
| Overleaf | HZDR |
| Redmine | HMGU |
| Rocket.Chat | Jülich |
| Storage (HDF) | DESY |
| webODV | AWI |

## Service Catalogue

| Service Name | Service Provider | Online since |
| ------ | ------ | ------ | 
| B2Share | Jülich | 29.03.2021 |
| bwSync&share (Nextcloud) | KIT | 29.03.2021 |
| Compute Projects | Jülich | 08.12.2021 |
| Container-Runtime | Jülich | 08.12.2021 |
| Data Projects | Jülich | 08.12.2021 |
| HAICORE | Jülich | 16.11.2021 |
| HAICORE | KIT | 04.10.2021 |
| Helmholtz Codebase (GitLab) | HZDR | 29.03.2021 |
| HIFIS Events (Indico) | DESY | 08.12.2021 |
| HIFIS Helpdesk (Zammad) | HZDR | 29.03.2021 |
| Jupyter on HAICORE | KIT | 08.12.2021 |
| Jupyter (JupyterHub) | DESY | 08.12.2021 |
| Jupyter (JupyterHub) | Jülich | 29.03.2021 |
| LimeSurvey | DKFZ | 16.11.2021 |
| LimeSurvey | HMGU | 30.11.2021 |
| Mattermost | HZDR | 29.03.2021 |
| Notes (HedgeDoc) | DESY | 10.12.2021 |
| nubes (Nextcloud) | HZB | 29.03.2021 |
| OpenStack (HDF Cloud) | Jülich  | 29.03.2021 |
| Rancher managed Kubernetes | DESY | 24.03.2022 |
| Singularity on HAICORE | KIT | 30.03.2022 |
| Sync & Share (Nextcloud) | DESY | 07.09.2021 |

## Retired Services

| Service Name | Service Provider | Online from... till... |
| ------ | ------ | ------ | 
| No retired services yet |  |  | 

