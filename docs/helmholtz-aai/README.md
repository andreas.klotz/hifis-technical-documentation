# Helmholtz AAI

This is the Helmholtz AAI Documentation, describing all you need to know about joining
as a user, a Virtual Organisation (VO), a service or as an identity provider.
We describe our goals, technology, organisation and policies.

For high-level representation, feel free to also have a look at the [Helmholtz AAI Homepage](https://aai.helmholtz.de).

## Goals

The goal of Helmholtz AAI is to enable stakeholders with a Helmholtz background to accomplish several tasks:

- Enable the participating Helmholtz Centres to provide services to
  well defined sets of federated users, based on solid authentication
  and authorisation.
- Enable Principal Investigators (VO Managers) at Helmholtz Centres to allocate
  resources on behalf of their group (VO) and to manage the authorisation
  for the members of their VOs.
- Enable global researchers to use services provided by Helmholtz
  Centres - given they are properly authorised and their identity is
  adequately understood.
- Be in line with European activities that focus around the European
  Open Science Cloud EOSC
