---
title: Service Changelog
---

## 2022-02-25

* Update Mattermost to version 6.3 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-3-is-now-available/)

## 2022-01-28

* Update Mattermost to version 6.2 -
  [Release Post](https://mattermost.com/blog/mattermost-v6-2-is-now-available/)

## 2022-01-14

* Update Mattermost to version 6.1 -
  [Release Post](https://www.hifis.net/news/2022/01/14/mattermost-6-available)

## 2021-11-30

* Update Mattermost to version 5.39 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-39/)
    * Looking ahead to general availability of 
      [Collapsed Reply Threads](https://mattermost.com/blog/mattermost-v5-39/#collapsed)

## 2021-10-01

* Update Mattermost to version 5.38 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-38/)
    * Enhanced user 
      [onboarding experience](https://mattermost.com/blog/mattermost-v5-38/#onboarding)

## 2021-08-26

* Update Mattermost to version 5.37 -
  [Release Post](https://mattermost.com/blog/mattermost-v5-37/)
    * Try out [Collapsed Reply Threads](https://mattermost.com/blog/collapsed-reply-threads-beta/)
    * Enable yourself in **Account Settings > Display > Collapsed Reply Threads (Beta)**.
