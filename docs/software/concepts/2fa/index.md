---
title: Two-Factor Authentication (2FA) for GitLab and Mattermost
---

# Two-Factor Authentication (2FA) for GitLab and Mattermost

## Getting Support

Before we dive into the topic of 2FA we would like to draw your attention to
possible contacts to get support from in case you are not able to set up 2FA by
yourself or if you have general questions about 2FA.
Your first contact is of course the HIFIS-Support which can be reached by
email; feel free to send your request or question to the HIFIS-HelpDesk via
[support@hifis.net](mailto:support@hifis.net).
Your second contact might be your colleagues of your local IT-department who
are familiar with the concept of 2FA and might be able to help you or answer
your questions as well.

## Brief Overview of 2AF

_Two-Factor Authentication (2FA)_ (authentication is the technical term for
logging in into an application) is a way to prove the identity of a user with
two different, independent components, so-called _factors_.
In 2FA, for example, these two factors could be "_something the user knows_"
(knowledge factor) like a secret password and "_something the user has_"
(possession factor) like a secret _One-Time Password (OTP)_ from a Mobile App
or a secret _Security Token_ from a USB-dongle.
2FA is known to be much more secure than just your usual credentials (your pair
of username and password) because an attacker needs to steal both the
credentials and the respective second factor.
There are two main approaches to create a second factor:

1. Mobile-Apps (like
[_FreeOTP_ :fontawesome-solid-up-right-from-square:](https://freeotp.github.io/)
from
[_Red Hat_ :fontawesome-solid-up-right-from-square:](https://www.redhat.com/en))
2. USB-Dongles (like 
[_YubiKey_ :fontawesome-solid-up-right-from-square:](https://www.yubico.com/products/)
from
[_Yubico_ :fontawesome-solid-up-right-from-square:](https://www.yubico.com/))

Many web-applications support 2FA, but in most cases it needs to be activated
explicitly by the user.
This is particularly the case for 
[_GitLab_ :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/)
and
[_Mattermost_ :fontawesome-solid-up-right-from-square:](https://mattermost.hzdr.de/).
We strongly encourage the usage of 2FA wherever possible.

In the following section we describe the first approach and provide
screenshots along the way.
For more detailed explanations you can of cause always consult the 
[GitLab user documentation :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/help/user/profile/account/two_factor_authentication.md).

## 2FA with Mobile Apps

First of all, you need to decide on which 2FA-mobile-app to install.
In this example we use
[_FreeOTP_ :fontawesome-solid-up-right-from-square:](https://freeotp.github.io/)
but you can use any other 2FA-mobile-app of your choice, such as:

- [Authy :fontawesome-solid-up-right-from-square:](https://authy.com/)
- [Duo Mobile :fontawesome-solid-up-right-from-square:](https://duo.com/product/multi-factor-authentication-mfa/duo-mobile-app)
- [LastPass Authenticator :fontawesome-solid-up-right-from-square:](https://lastpass.com/auth/)
- [Authenticator :fontawesome-solid-up-right-from-square:](https://mattrubin.me/authenticator/)
- [andOTP :fontawesome-solid-up-right-from-square:](https://github.com/andOTP/andOTP)
- [Google Authenticator :fontawesome-solid-up-right-from-square:](https://support.google.com/accounts/answer/1066447?hl=en)
- [Microsoft Authenticator :fontawesome-solid-up-right-from-square:](https://www.microsoft.com/en-us/security/mobile-authenticator-app)
- [SailOTP :fontawesome-solid-up-right-from-square:](https://openrepos.net/content/seiichiro0185/sailotp)
- [Synology Secure SignIn :fontawesome-solid-up-right-from-square:](https://play.google.com/store/apps/details?id=com.synology.securesignin&gl=US)

_FreeOTP_ supports two different implementations of One-Time Password (OTP)
protocols which are _HOTP_ - HMAC-based One-Time Password - and _TOTP_ - 
Time-based One-Time Password.

On Android devices the _FreeOTP_ user-interface looks much like the following:

![FreeOTP screenshot](images/android-small.png)

We can see that in the upper right corner of the screen there are two buttons,
one displaying a
[QR code :fontawesome-solid-up-right-from-square:](https://en.wikipedia.org/wiki/QR_code)
and one that displays a key.
You can click the button to scan a QR code, which sets all configurations automatically.

For both
[_GitLab_ :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/)
and
[_Mattermost_ :fontawesome-solid-up-right-from-square:](https://mattermost.hzdr.de/)
you need to enable 2FA from within your 
[_GitLab_ user account settings :fontawesome-solid-up-right-from-square:](https://codebase.helmholtz.cloud/-/profile/account).
To get a QR code to be scanned with your mobile-app you need to enable 2FA in
_GitLab_ by clicking the "_Enable two-factor-authentication_" button.

![2FA screenshot](images/screenshot-gitlab-enable-2fa-small.png)

As a results a QR code similar to the following will be displayed:

![QR code screenshot](images/screenshot-gitlab-enable-2fa-scan-qr-small.png)

Now with the QR code at hand you can scan it with your 2FA-mobile-app of your
choice.
As a result a new entry will show up in your app.
You can tap on the entry and get your pin code which you need to enter into the
pin code input field in _GitLab_.
As soon as you click the button "_Register with two-factor app_" the
registration is done and the 2FA is activated.

Finally, you will be provided with a set of recovery codes that you are asked
to copy, download or print, securely store them somewhere and click the
"_Proceed_" button.

![Recovery codes screenshot](images/screenshot-gitlab-enable-2fa-recovery-codes-small.png)

From now on, every time you log in into GitLab or Mattermost with your
credentials you will be asked to generate a pin code with your 2FA-mobile-app
and enter it in the respective input field.

![Pin code screenshot](images/screenshot-gitlab-login-pin-code-small.png)
