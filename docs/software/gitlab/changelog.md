---
title: Changelog
---

## 2022-03-29

* Update GitLab to version 14.9
    * All details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/03/22/gitlab-14-9-released/)

## 2022-03-01

*  Update GitLab to version 14.8
    * Add default issue and merge request
      [templates](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#add-default-issue-and-merge-request-templates-in-a-projects-repository)
      in a project’s repository
    * [Improve pipeline index page layout](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#improve-pipeline-index-page-layout)
    * [Latest Release badge for the project](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/#latest-release-badge-for-the-project)
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/02/22/gitlab-14-8-released/)

## 2022-02-07

* Update GitLab to version 14.7
    * Create
      [group access tokens](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/#group-access-tokens)
      using the UI and API
    * Major
      [Gitleaks](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/#major-gitleaks-performance-improvements)
      performance improvements
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2022/01/22/gitlab-14-7-released/)

## 2022-01-12

* Update GitLab to version 14.6
    * Copy code blocks in Markdown with a single
      [click](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released/#copy-code-blocks-in-markdown-with-a-single-click)
    * Toggle between editing in raw Markdown and WYSIWYG when editing
      [wiki pages](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released/#toggle-wiki-editors-seamlessly)
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/12/22/gitlab-14-6-released/)

## 2021-12-03

* Update GitLab to version 14.5
    * Introducing Infrastructure as Code (IaC)
      [security scanning](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#introducing-infrastructure-as-code-iac-security-scanning)
    * Cleaner diffs for
      [Jupyter Notebook files](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/#cleaner-diffs-for-jupyter-notebook-files)
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/11/22/gitlab-14-5-released/)

## 2021-11-22

* Update GitLab to version 14.4
    * Remote Repositories for GitLab in 
      [Visual Studio Code](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released/#remote-repositories-for-gitlab-in-visual-studio-code)
    * Integrated 
      [error tracking](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released/#integrated-error-tracking-inside-gitlab-without-a-sentry-instance) 
      inside GitLab without a Sentry instance
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/10/22/gitlab-14-4-released)

## 2021-10-05

* Update GitLab to version 14.3
    *  Introduces further improvements in the
       [wiki editor](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/#edit-a-tables-structure-visually-in-the-new-wiki-editor)
    * Allows to
      [use variables in other variables](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/#use-variables-in-other-variables)
      in `gitlab-ci.yml`
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/09/22/gitlab-14-3-released/)

## 2021-08-27

* Update GitLab to version 14.2
    * Introduces [stageless pipelines](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/#stageless-pipelines).
    * Adds a [Markdown live preview](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/#preview-markdown-live-while-editing)
      to the Web IDE and single file editor to reduce the need for context switches.
    * More details are available in the
      [Release Post](https://about.gitlab.com/releases/2021/08/22/gitlab-14-2-released/)

## 2021-08-09

* Update GitLab to version 14.1 -
  [Release Post](https://about.gitlab.com/releases/2021/07/22/gitlab-14-1-released/)
