# AAI
## OpenID Connect configuration
The HZDR GitLab integrates with the Helmholtz AAI using OpenID Connect.
Therefore, the variable `gitlab_rails['omniauth_providers']` is
configured in `/etc/gitlab/gitlab.rb`.

```ruby
gitlab_rails['omniauth_providers'] = [
  {
    'name' => 'openid_connect',
    'label' => 'Helmholtz AAI',
    'icon' => 'helmholtz.png',
    'args' => {
      'name' => 'openid_connect',
      'scope' => ['openid','profile', 'email', 'eduperson_principal_name'],
      'response_type' => 'code',
      'issuer' => 'https://login.helmholtz.de/oauth2',
      'discovery' => true,
      'client_auth_method' => 'basic',
      'uid_field' => 'eduperson_principal_name',
      'send_scope_to_token_endpoint' => 'true',
      'client_options' => {
        'identifier' => 'CHANGEME',
        'secret' => 'CHANGEME',
        'redirect_uri' => 'https://{ GITLAB_DOMAIN }/users/auth/openid_connect/callback'
      }
    }
  },
]
```
At the same time, _Omniauth_ is enabled to allow single
sign-on via the configured provider.
```ruby
gitlab_rails['omniauth_enabled'] = true
gitlab_rails['omniauth_allow_single_sign_on'] = ['openid_connect']
```
Depending on your requirements you might want users who sign in via the
Helmholtz AAI to become
[external users](https://docs.gitlab.com/ee/user/permissions.html#external-users-core-only)
by default.
This option is configurable in `/etc/gitlab/gitlab.rb` as well.
```ruby
gitlab_rails['omniauth_external_providers'] = ['openid_connect']
```
Further information is available in the
[GitLab documentation](https://docs.gitlab.com/ee/administration/auth/oidc.html).
