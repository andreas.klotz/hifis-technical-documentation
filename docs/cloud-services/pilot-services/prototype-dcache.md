# The dCache Pilot Service

The dCache storage service at DESY was one of the first services connected to the Helmholtz AAI.

To log in, you will be redirected to the Helmholtz AAI instance Unity@FZJ which will redirect you to your home IdP for proper authentication.

## dCache specifics

- As mentioned, this is a pilot service at the moment.
    - Please keep in mind that system administrators theoretically have access to your data, so please don't store anything confidential.
    - It is clear on top, the usual rules of the Helmholtz Association and DESY apply concerning the use of public storage space.
    - We don't make a backup of this service yet as it is only to prove a functional AAI. So if something breaks we will probably not be able to restore your data.
- Accessing the dCache link <https://dcache-demo.desy.de> you will see the GUI in 'browsing only' mode as you didn't log-in yet. Any attempt to download or upload data will fail.
- Please follow the (still lengthy) procedure below to become authorized for the storage service.
- As soon as you can log in, 'click' the 'home button' (left top corner) to access your private storage area.
- Have fun!

## How to get Authorised

As the service is not yet fully integrated, you need to manually apply for authorization and correct user id mapping. 
This tutorial guides you through the process to get access to the dCache storage service.

!!! warning
    Please follow the instructions, otherwise you will end up with a refused access.

### Apply for usage and provide your Personal Identifier

1. Go to <https://login.helmholtz.de/home/>.
2. Login with your home institution's user account.
    - Choose your organisation. You can also search for parts of the name or abbreviation, e.g., "DLR"
    - You'll be forwarded to login via you home organisation's identity provider.
3. Then you can see all your Helmholtz AAI data:  
   ![Unity Login Home Screen](images/unity-home.png)
4. Identify and copy the string that comes after "_Anonymous identifier_". It is a lengthy code like "afe4bc2f-3ce0-4920-a693-f64445f76f56".
5. Send a request to <support@hifis.net> and provide some basic information to us:
    - Who is the responsible person for usage of the service
    - What purpose would you like to use it for
    - Copy-paste the abovementioned Anonymous identifier
6. Wait for our response.


## Accessing the dCache service

Once access is approved,
please follow this URL <https://dcache-demo.desy.de> and make sure you specify **https**. It won't work with plain 'http'.
Once there, click on the top-right 'Log in' button.

![AAI-dcache-not-logged-in](images/AAI-dcache-not-logged-in.png){ width=50% }

------------------------------------------------------------------------

### Select OIDC as your login mechanism

You will be redirected to the dCache log-in page, offering two authentication mechanisms:
- Local Login with user / password
- OpenID Connect
**Please choose OpenID Connect**

![AAI-dcache-login-via-oidc.png](images/AAI-dcache-login-via-oidc.png){ width=50% }

------------------------------------------------------------------------

### Choosing between ESCAPE and Helmholtz AAI

Choosing OpenID Connect will redirect you to a list of possible OpenID Connect Community Proxies. Currently dCache offers four of those:

- The H2020 ESCAPE Project Proxy, operated at INFN and running the INDIGO IAM.
- The Helmholtz AAI Unity service, operated by FZJ, Juelich.
- EGI Checkin
- DESY Keycloak Proxy Service

**Helmholtz Members: please select “Login via Helmholtz”**:

![AAI-choose-ESCAPE-HDF.png](images/AAI-choose-Helmholtz.png){ width=50% }

------------------------------------------------------------------------

### Find your home IdP

*If you already have an active login with the HDF AAI, you won't see the following two screens and you can proceed directly with the step [Data Privacy Confirmation](#data-privacy-confirmation).*

Otherwise you are redirected to the Unity HDF community proxy in Juelich, which allows you to use a variety of mechanisms to identify yourself:

- Local login to Unity.
- Using the ORCID or GITHUHB account (IdP)
- Using your home IdP.

Helmholtz Members please select your home institute in the list menu on the right hand side of the page. *There is a convenient search box on top of the list menu.*

![AAI-dcache-choose-IDPinUnity.png](images/AAI-dcache-choose-IDPinUnity.png){ width=75% }

------------------------------------------------------------------------

### Identify against your home IdP

Unity will now redirect you to your home Identity Provider, which will look different for each Helmholtz Centre.
The picture below shows how it looks like for DESY.

Once there, Please log-in with your Center specific credentials.

![AAI-dcache-loginto-your-IDP.png](images/AAI-dcache-loginto-your-IDP.png){ width=50% }

------------------------------------------------------------------------

### Data Privacy Confirmation

As your Helmholtz Centre IdP is now releasing some information to Unity, the Data Privacy law requires that we ask you for your permission to do so.
So the next page will provide you with all the information released to Unity.

Hit 'Confirm'.

------------------------------------------------------------------------

## Have fun!

If anything breaks, contact us (again) at <support@hifis.net>.
