# Documentation on Helmholtz Cloud Service Operation KPI calculation

This is to document the calculcation of the Service Operation KPI (WP 1.4) for Helmholtz Cloud (HC) services by the Cloud Cluster Management of HIFIS. For further information, see [https://hifis.net](https://hifis.net) and [Original platform proposal for HIFIS](https://www.helmholtz.de/fileadmin/user_upload/01_forschung/Helmholtz_Inkubator_HIFIS.pdf), Page 56 about WP 1.4.

All numeric information are collected in a common public repository at
<https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci>.

[The full calculation is described here](kpi-calculation.pdf).
