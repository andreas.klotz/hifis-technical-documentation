# Documentation on Helmholtz Cloud Service Operation KPI calculation

This is to document the calculcation of the Service Operation KPI (WP 1.4) for Helmholtz Cloud (HC) services by the Cloud Cluster Management of HIFIS. For further information, see https://hifis.net and [Original platform proposal for HIFIS](https://www.helmholtz.de/fileadmin/user_upload/01_forschung/Helmholtz_Inkubator_HIFIS.pdf), Page 56 about WP 1.4.

All numeric information are collected in a common public repository at 
<https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci>.

## Calculation Period: Overall _and_ Annual Reporting
HC Operation KPI are calculated in two ways:

I.) from service start in [Helmholtz Cloud Portal](https://helmholtz.cloud/services) to reporting date
II.) from beginning of current reporting period (beginning of year) to reporting date (end of year).

For the reporting year 2021, we focus on variant I) only.

## Definition of time points
### Starting times
Due to the heterogeneity of HC services onboarding time points, as well as the KPI reporting frequencies, multiple starting time points are defined to suit all use cases.

$$
T_{\rm fitstart\_overall}=\left(\begin{matrix}T_{\rm onboard} & \iff T_{\rm onboard} \geq T_{\rm logstart}\\T_{\rm logstart}&\rm else\end{matrix}\right.
$$
$$
T_{\rm reference\_overall}=\left(\begin{matrix}T_{\rm fitstart} & \iff f\left(T_{\rm fitstart}\right)\gg 0\\T_{\rm fitstart,manually\ defined}&\rm else\end{matrix}\right.
$$

with

- _T_~onboard~ denoting the time of [public availability of the service in the HC](https://gitlab.hzdr.de/hifis/overall/kpi/helmholtz-cloud-services/-/blob/main/stats/cloud-services.csv).
- _T_~logstart~ denoting the time of the first data point in the [reported service KPI data](https://gitlab.hzdr.de/hifis/overall/kpi).

_Note:_ The potential definition of a needed _T_~fitstart,manually\ defined~ value is subject to the HC management, and is defined in the KPI computation repository.

### End/Reporting time 
The reporting time point _T_~report~ is simply defined as the date-time of the end of the considered time frame (e.g., 2021-12-31 for the 2021 annual report).

## Linear regression
From all available KPI data, a linear fit is calculated with linear regression (based on [gnuplot fit function](http://www.gnuplot.info/docs_5.4/Gnuplot_5_4.pdf), implementing nonlinear least-squares (NLLS) Marquardt-Levenberg algorithm), yielding for each service _s_ and each reported user or usage number _u_:
- A slope:
  $$ m\left(s,u\right) \pm m_{\rm err}\left(s,u\right)$$
- Intercept:
  $$ y_0\left(s,u\right) \pm y_{0,\rm err}\left(s,u\right) $$

## KPI Calculation

### Raw KPI components
Overall HC KPI is composed out of an arbitrary number of raw KPI components and respective weights. This depicts the contribution of single services and single service usage information towards the overall development of the HC.

The raw KPI components _K_ are calculated independently for each _s_ and _u_, at the reporting time, based on linear fit calculated before:
$$
K\left(s,u,T_{\rm report}\right)=\frac{m\left(s,u\right)T_{\rm report}+y_0\left(s,u\right)}{m\left(s,u\right)T_{\rm reference\_overall}+y_0\left(s,u\right)}-1
$$

#### Side conditions
- To avoid calculation from sparse data of too fresh services, raw KPI calculation is only performed if 
$$
T_{\rm report} \geq T_{\rm reference\_overall}+90{\ \rm d}
$$

### Weighting
Each [raw KPI component is weighted](https://gitlab.hzdr.de/hifis/overall/kpi/kpi-plots-ci/-/blob/service-usage-2020-21/data/kpi-weights.csv) with a pre-defined weight _w~s,u~_.  
All services are of equal value; the weighting of all raw data of a service is 1 in total.  
If applicable and reasonable, the weighting of the raw usage data is 50% , as well as the weighting of the raw user data is 50%.

### Cumulated KPI
Overall KPI _K~o~_ is calculated by sum of all weighted KPI components:
$$
K_o\left(T_{\rm report}\right)=\sum_{s,u}{w_{s,u}K\left(s,u,T_{\rm report}\right)}
$$
