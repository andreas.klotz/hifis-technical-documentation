# What is HIFIS?
*Source: HIFIS Website*

The top position of Helmholtz research is increasingly based on cross-centre and international cooperation and common access to data treasure and -services. At the same time the significance of a sustainable software development for the research process is recognised.
HIFIS (Helmholtz Infrastructure for Federated ICT Services) aims to ensure an excellent information environment for outstanding research in all Helmholtz research fields and a seamless and performant IT-infrastructure connecting knowledge from all centres. It will build a secure and easy-to-use collaborative environment with efficiently accessible ICT services from anywhere. HIFIS will also support the development of research software with a high level of quality, visibility and sustainability.
To achieve its goals, HIFIS works in three Competence Clusters which are distributed throughout different Helmholtz research centers:

* Cloud Services: Federated platform for proven first class Cloud Services; led by HZB
* Backbone Services: High-performance trusted network infrastructure with unified basic services; led by DESY
* Software Services: Platform, training and support for high-quality, sustainable software development, led by HZDR

The Service Portfolio Management is part of the Cloud Services cluster which sets about the federated platform called “Helmholtz Cloud”.
