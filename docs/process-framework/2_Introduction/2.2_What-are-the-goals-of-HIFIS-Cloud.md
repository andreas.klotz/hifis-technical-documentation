# What are the goals of HIFIS Cloud?
*Source: HIFIS Website*

The specific aim of the Cloud cluster (HIFIS Cloud) is to provide the Helmholtz Cloud as a federated Cloud Platform. Helmholtz Cloud offers services to the entire scientific community and partners in the long tail of science - not just selected large-scale projects. It thus complements the federal support services for large-scale projects both in breadth (support for projects of all sizes) and depth (focusing on the software and service level) and provides other platform topics with an access level for the services arising there.
In particular, the Cloud cluster is developing the technologies for access, harmonization, integration and provisioning of the services integrated into the Helmholtz Cloud network, and will make them available. It thus is creating the organisational and technical framework to open up a broad portfolio of services to all users of the Helmholtz Association and beyond through self-organisation. The connection to international developments (e.g. EOSC at European level) is regarded as essential and the “FAIR” principles in particular must be taken into account.

